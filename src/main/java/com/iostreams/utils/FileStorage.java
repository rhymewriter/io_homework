package com.iostreams.utils;

import java.io.File;
import java.io.IOException;

public class FileStorage {

    public static final File FOLDER_NAME;
    public static final File OLD_FILE_1;
    public static final File OLD_FILE_2;
    public static final File OLD_FILE_3;
    public static final File NEW_FILE_1;
    public static final File NEW_FILE_2;
    public static final File NEW_FILE_3;

    static {
        FOLDER_NAME = new File("DataFiles");
        if (!FOLDER_NAME.exists()) {
            FOLDER_NAME.mkdir();
        }

        OLD_FILE_1 = new File("DataFiles\\old_data_1.txt");
        OLD_FILE_2 = new File("DataFiles\\old_data_2.txt");
        OLD_FILE_3 = new File("DataFiles\\old_data_3.txt");
        NEW_FILE_1 = new File("DataFiles\\new_data_1.txt");
        NEW_FILE_2 = new File("DataFiles\\new_data_2.txt");
        NEW_FILE_3 = new File("DataFiles\\new_data_3.txt");

        try{
            if (!OLD_FILE_1.exists()){
                OLD_FILE_1.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        try{
            if (!OLD_FILE_2.exists()){
                OLD_FILE_2.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        try{
            if (!OLD_FILE_3.exists()){
                OLD_FILE_3.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        try{
            if (!NEW_FILE_1.exists()){
                NEW_FILE_1.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        try{
            if (!NEW_FILE_2.exists()){
                NEW_FILE_2.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        try{
            if (!NEW_FILE_3.exists()){
                NEW_FILE_3.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}