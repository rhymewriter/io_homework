package com.iostreams;

import com.iostreams.services.FileServices;
import static com.iostreams.utils.FileStorage.*;
import com.iostreams.wrappers.BufferedReaderWrapper;
import com.iostreams.wrappers.FileWriterWrapper;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        String oldFile1Text = "Past file content 1";
        String oldFile2Text = "Past file content 2";
        String oldFile3Text = "Past file content 3";
        String newFile1Text = "New file content 1";
        String newFile2Text = "New file content 2";
        String newFile3Text = "New file content 3";

        try (FileWriter oldFile1Writer = new FileWriter(OLD_FILE_1);
             FileWriter oldFile2Writer = new FileWriter(OLD_FILE_2);
             FileWriter oldFile3Writer = new FileWriter(OLD_FILE_3);
             FileWriter newFile1Writer = new FileWriter(NEW_FILE_1);
             FileWriter newFile2Writer = new FileWriter(NEW_FILE_2);
             FileWriter newFile3Writer = new FileWriter(NEW_FILE_3)) {
            oldFile1Writer.write(oldFile1Text);
            oldFile2Writer.write(oldFile2Text);
            oldFile3Writer.write(oldFile3Text);
            newFile1Writer.write(newFile1Text);
            newFile2Writer.write(newFile2Text);
            newFile3Writer.write(newFile3Text);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReaderWrapper bufferedReaderWrapper = new BufferedReaderWrapper();
        FileWriterWrapper fileWriterWrapper = new FileWriterWrapper();

        FileServices fileServices = new FileServices(bufferedReaderWrapper, fileWriterWrapper);
        String[] fileNames = {"DataFiles\\old_data_1.txt", "DataFiles\\old_data_2.txt", "DataFiles\\old_data_3.txt"};
        String allText = fileServices.readFiles(fileNames);

        fileServices.writeFileWithDelete(allText, "DataFiles\\new_data_1.txt");
        fileServices.writeFileToStart(allText, "DataFiles\\new_data_2.txt");
        fileServices.writeFileToEnd(allText, "DataFiles\\new_data_3.txt");
    }
}
